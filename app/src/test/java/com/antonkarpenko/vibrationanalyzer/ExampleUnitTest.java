package com.antonkarpenko.vibrationanalyzer;

import com.antonkarpenko.vibrationanalyzer.manager.ActionManager;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void checkFileName() {
        ActionManager manager = new ActionManager();
        System.out.println(manager.composeFileName());
    }
}