package com.antonkarpenko.vibrationanalyzer.model;

import android.bluetooth.BluetoothSocket;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Exchanger;

/**
 * Created by Антон on 25.04.2019.
 */
public class ConnectedThread extends Thread {

    private final InputStream inputStream;
    private BluetoothSocket bluetoothSocket;
    private Exchanger<Package> packageExchanger;

    private Package aPackage;
    private FileWriter writer;

    private static final String LOGGER_TAG = "Thread";
    private static final int BYTES_AMOUNT = 5;

    public ConnectedThread(BluetoothSocket bluetoothSocket, Exchanger<Package> packageExchanger, String fileName) {
        InputStream inputStream1 = null;
        this.bluetoothSocket = bluetoothSocket;
        this.aPackage = new Package();
        this.packageExchanger = packageExchanger;
        try {
            inputStream1 = bluetoothSocket.getInputStream();
            this.writer = new FileWriter(new File(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        inputStream = inputStream1;
    }

    public Package getaPackage() {
        return aPackage;
    }

    public void setaPackage(Package aPackage) {
        this.aPackage = aPackage;
    }

    public Exchanger<Package> getPackageExchanger() {
        return packageExchanger;
    }

    public void setPackageExchanger(Exchanger<Package> packageExchanger) {
        this.packageExchanger = packageExchanger;
    }

    public BluetoothSocket getBluetoothSocket() {
        return bluetoothSocket;
    }

    public void setBluetoothSocket(BluetoothSocket bluetoothSocket) {
        this.bluetoothSocket = bluetoothSocket;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public FileWriter getWriter() {
        return writer;
    }

    public void setWriter(FileWriter writer) {
        this.writer = writer;
    }

    @Override
    public void run() {
        byte[] buffer = new byte[BYTES_AMOUNT];
        try {
            writer.write(String.format("%s   %s    %s\\n", "X", "Y", "SUM"));
            while (bluetoothSocket.isConnected()) {
                inputStream.read(buffer);
                createPackageAndSend(buffer);
                writer.write(getStringForFile(aPackage));
                Thread.sleep(5);
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            closeWriter();
            closeInputStream();
        }
    }

    private void createPackageAndSend(byte[] buf){

        // get length of received buf
        int len = buf.length;
        // check length
        if (len < BYTES_AMOUNT || (buf[0] & 0x80) == 0) {
            return;
        }
    	System.out.println(String.format("%02X %02X %02X", buf[0], buf[1], buf[2]));

        // extract byte #1
        int val1 = (buf[1] + ((buf[0] & 0x01) << 7));
        // extract byte #2
        val1 += (buf[2] + ((buf[0] & 0x02) << 6))*256;

        if ((val1 & 0x8000) > 0) val1 -= 65536;

    	System.out.println(String.format("%d", val1));

        // extract byte #3
        int val2 = (buf[3] + ((buf[0] & 0x04) << 5));
        // extract byte #4
        val2 += (buf[4] + ((buf[0] & 0x08) << 4))*256;

        if ((val2 & 0x8000) > 0) val2 -= 65536;

    	System.out.println(String.format("%d", val2));
        aPackage.setX(val1);
        aPackage.setY(val2);
        try {
            packageExchanger.exchange(aPackage);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private String getStringForFile(Package aPackage) {
        return String.format("%s   %s    %s\\n", aPackage.getX(), aPackage.getY(), aPackage.getSum());
    }

    public void closeWriter() {
        try {
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void closeInputStream() {
        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
