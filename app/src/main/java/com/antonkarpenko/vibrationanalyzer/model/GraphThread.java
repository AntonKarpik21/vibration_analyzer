package com.antonkarpenko.vibrationanalyzer.model;

import android.os.Handler;

import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.concurrent.Exchanger;

/**
 * Created by Антон on 23.05.2019.
 */
public class GraphThread implements Runnable {

    private Exchanger<Package> packageExchanger;
    private LineGraphSeries<DataPoint> lineGraphSeriesX;
    private LineGraphSeries<DataPoint> lineGraphSeriesY;
    private final Handler handler = new Handler();
    private Package aPackage;

    private static int pixel = 0;

    public GraphThread(Exchanger<Package> packageExchanger,
                       LineGraphSeries<DataPoint> lineGraphSeriesX,
                       LineGraphSeries<DataPoint> lineGraphSeriesY) {

        this.packageExchanger = packageExchanger;
        this.lineGraphSeriesX = lineGraphSeriesX;
        this.lineGraphSeriesY = lineGraphSeriesY;
        pixel = 0;
    }

    @Override
    public void run() {
        try {
            aPackage = packageExchanger.exchange(null);
            if (aPackage != null) {
                lineGraphSeriesX.appendData(new DataPoint(pixel, aPackage.getX()), true, 40);
                lineGraphSeriesY.appendData(new DataPoint(pixel, aPackage.getY()), true, 40);
                handler.postDelayed(this, 5);
                pixel++;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
