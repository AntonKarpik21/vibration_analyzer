package com.antonkarpenko.vibrationanalyzer.model;

import static java.lang.Math.signum;
import static java.lang.Math.sqrt;

/**
 * Created by Антон on 23.05.2019.
 */
public class Package {
    private double x;
    private double y;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getSum() {
        return sqrt(x*x + y*y) * signum(y);
    }
}
