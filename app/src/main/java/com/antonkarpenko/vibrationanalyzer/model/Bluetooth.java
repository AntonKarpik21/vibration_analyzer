package com.antonkarpenko.vibrationanalyzer.model;

import java.io.Serializable;

/**
 * Created by Антон on 07.02.2019.
 */
public class Bluetooth implements Serializable {

    private String address;
    private String name;
    private int scanMode;
    private int state;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScanMode() {
        return scanMode;
    }

    public void setScanMode(int scanMode) {
        this.scanMode = scanMode;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "Bluetooth{" +
                "address='" + address + '\'' +
                ", name='" + name + '\'' +
                ", scanMode=" + scanMode +
                ", state=" + state +
                '}';
    }
}
