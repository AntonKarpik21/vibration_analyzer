package com.antonkarpenko.vibrationanalyzer.manager;

import com.google.gson.Gson;

/**
 * Created by Антон on 07.02.2019.
 */
public class JsonMapper {

    private Gson gson;

    public JsonMapper(Gson gson) {
        this.gson = gson;
    }

    /**
     * to json
     * @param var
     * @return
     */
    public String map(Object var) {
        return gson.toJson(var);
    }

    /**
     * from json to object T
     * @param json
     * @param clazz
     * @param <T>
     * @return
     */
    public <T> T map(String json, Class<T> clazz) {
        return gson.fromJson(json, clazz);
    }
}
