package com.antonkarpenko.vibrationanalyzer.manager.factories;

import android.bluetooth.BluetoothAdapter;

import com.antonkarpenko.vibrationanalyzer.model.Bluetooth;

/**
 * Created by Антон on 07.02.2019.
 */
public class ResourceFactory {

    public Bluetooth createBluetooth(BluetoothAdapter adapter) {
        Bluetooth bluetooth = new Bluetooth();

        bluetooth.setAddress(adapter.getAddress());
        bluetooth.setName(adapter.getName());
        bluetooth.setScanMode(adapter.getScanMode());
        bluetooth.setState(adapter.getState());

        return bluetooth;
    }
}
