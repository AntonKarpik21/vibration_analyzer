package com.antonkarpenko.vibrationanalyzer.manager;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Parcelable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.antonkarpenko.vibrationanalyzer.BluetoothActivity;
import com.antonkarpenko.vibrationanalyzer.GraphActivity;
import com.antonkarpenko.vibrationanalyzer.manager.factories.ResourceFactory;
import com.antonkarpenko.vibrationanalyzer.model.Bluetooth;
import com.antonkarpenko.vibrationanalyzer.model.ConnectedThread;
import com.antonkarpenko.vibrationanalyzer.model.Package;
import com.jjoe64.graphview.series.DataPoint;

import java.io.IOException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Exchanger;

import javax.inject.Inject;

import static android.app.ProgressDialog.STYLE_HORIZONTAL;
import static android.bluetooth.BluetoothAdapter.ACTION_DISCOVERY_FINISHED;
import static android.bluetooth.BluetoothAdapter.ACTION_DISCOVERY_STARTED;
import static android.bluetooth.BluetoothDevice.ACTION_FOUND;
import static android.bluetooth.BluetoothDevice.EXTRA_DEVICE;
import static android.content.Context.MODE_PRIVATE;
import static com.antonkarpenko.vibrationanalyzer.R.string.PAIRED_DEVICES;
import static com.antonkarpenko.vibrationanalyzer.R.string.SCANNING;
import static com.antonkarpenko.vibrationanalyzer.R.string.SCANNING_MESSAGE;
import static com.antonkarpenko.vibrationanalyzer.constants.Properties.BLUETOOTH_PARAMETERS;
import static com.antonkarpenko.vibrationanalyzer.constants.Properties.getBluetoothParameters;
import static com.antonkarpenko.vibrationanalyzer.constants.Properties.saveBluetoothParameters;
import static com.antonkarpenko.vibrationanalyzer.manager.MessageManager.BLUETOOTH_ENABLED;
import static com.antonkarpenko.vibrationanalyzer.manager.MessageManager.BLUETOOTH_NOT_SUPPORTED;
import static com.antonkarpenko.vibrationanalyzer.manager.MessageManager.NO_PREFERENCE_PARAMETER;
import static com.antonkarpenko.vibrationanalyzer.manager.ViewManager.enableView;
import static com.antonkarpenko.vibrationanalyzer.manager.ViewManager.exchangeVisibility;
import static com.antonkarpenko.vibrationanalyzer.manager.ViewManager.setTextToTextView;

/**
 * Created by Антон on 06.02.2019.
 */
public class ActionManager {

    private Activity activity;
    private BluetoothAdapter bluetoothAdapter;
    private SharedPreferences sharedPreferences;
    private String loggerTag;
    private BroadcastReceiver broadcastReceiver;
    private ConnectedThread connectedThread;
    private BluetoothSocket bluetoothSocket;

    @Inject
    JsonMapper mapper;

    @Inject
    ResourceFactory resourceFactory;

    public BluetoothAdapter getBluetoothAdapter() {
        return bluetoothAdapter;
    }

    public void setBluetoothAdapter(BluetoothAdapter bluetoothAdapter) {
        this.bluetoothAdapter = bluetoothAdapter;
    }

    public String getLoggerTag() {
        return loggerTag;
    }

    public void setLoggerTag(String loggerTag) {
        this.loggerTag = loggerTag;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    public void setSharedPreferences(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public BroadcastReceiver getBroadcastReceiver() {
        return broadcastReceiver;
    }

    public void setBroadcastReceiver(BroadcastReceiver broadcastReceiver) {
        this.broadcastReceiver = broadcastReceiver;
    }

    public void startDevicesDiscovery() {
        if (bluetoothAdapter.isDiscovering()) {
            bluetoothAdapter.cancelDiscovery();
        }
        int MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1;
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION);

        bluetoothAdapter.startDiscovery();
    }

    public void exit() {
        activity.finish();
        System.exit(0);
    }

    public void initBluetoothAdapter() {
        setBluetoothAdapter(BluetoothAdapter.getDefaultAdapter());
    }

    public void setScanningParameters(ArrayList<BluetoothDevice> devices) {
        setScanBluetoothDevicesParameters(devices);
    }

    public void checkBluetoothActivity(Button enableButton,
                                       Button scanButton,
                                       TextView topLabel,
                                       String newText) {

        if (bluetoothAdapter != null && bluetoothAdapter.isEnabled()) {
            setTextToTextView(topLabel, newText);
            exchangeVisibility(enableButton, scanButton);
            saveBluetooth();
        } else {
            enableView(enableButton);
            Log.i(loggerTag, "Bluetooth is disabled or doesn't support");
        }
    }

    public void activateBluetooth(Context context) {
        if (bluetoothAdapter == null) {
            // Device doesn't support BluetoothActivity
            showMessage(context, BLUETOOTH_NOT_SUPPORTED);
        } else {
            if (!bluetoothAdapter.isEnabled()) {
                Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                activity.startActivityForResult(intent, 1000);
                showMessage(context, BLUETOOTH_ENABLED);
                saveBluetooth();
            }
        }
    }

    public void enableBluetoothClick(Context context,
                                     TextView topLabel,
                                     String newText,
                                     Button enableButton,
                                     Button scanButton,
                                     Button pairedDevicesButton) {

        activateBluetooth(context);

        setTextToTextView(topLabel, newText);
        exchangeVisibility(enableButton, scanButton);
        enableView(pairedDevicesButton);
    }

    public void showPairedDevices(Context context) {
        Set<BluetoothDevice> bluetoothDevices = bluetoothAdapter.getBondedDevices();

        if (bluetoothDevices == null || bluetoothDevices.size() == 0) {
            showMessage(context, "No Paired Devices Found");
        } else {
            ArrayList<BluetoothDevice> devicesList = new ArrayList<>();
            devicesList.addAll(bluetoothDevices);
            Intent intent = new Intent(activity, BluetoothActivity.class);
            intent.putParcelableArrayListExtra("device.list", devicesList);

            activity.startActivity(intent);
        }
    }

    public void initSharedPreferences() {
        setSharedPreferences(activity.getPreferences(MODE_PRIVATE));
    }

    private void saveBluetooth() {
        SharedPreferences preferences = getSharedPreferences();
        Bluetooth bluetooth;
        if (preferences.getString(BLUETOOTH_PARAMETERS, null) == null) {
            bluetooth = resourceFactory.createBluetooth(bluetoothAdapter);
            saveBluetoothParameters(mapper.map(bluetooth), preferences);
            Log.i(loggerTag, "Bluetooth parameters: " + bluetooth);
        } else {
            Log.i(loggerTag, "There are no bluetooth parameters");
        }
    }

    public void initBluetoothActivity(TextView bluetoothName,
                                      TextView bluetoothAddress,
                                      TextView bluetoothScanMode,
                                      TextView bluetoothState,
                                      Context context) {

        SharedPreferences preferences = getSharedPreferences();
        Log.i(loggerTag, "Preferences: " + preferences);
        String btParameter = getBluetoothParameters(preferences);
        Log.i(loggerTag, "Bluetooth parameters: " + btParameter);

        if (btParameter != null) {
            Bluetooth bluetooth = mapper.map(btParameter, Bluetooth.class);

            bluetoothName.setText(bluetooth.getName());
            bluetoothAddress.setText(bluetooth.getAddress());
            bluetoothScanMode.setText(String.valueOf(bluetooth.getScanMode()));
            bluetoothState.setText(String.valueOf(bluetooth.getState()));
        } else {
            MessageManager.showMessage(context, NO_PREFERENCE_PARAMETER, BLUETOOTH_PARAMETERS);
        }
    }

    public void setScanBluetoothDevicesParameters(ArrayList<BluetoothDevice> bluetoothDevices) {

        Log.i(loggerTag, "Beginning of scanning");
        clearFoundBluetoothDevices(bluetoothDevices);
        ProgressDialog progressDialog = getProgressDialog(activity.getString(SCANNING), activity.getString(SCANNING_MESSAGE));
        broadcastReceiver = getBroadcastReceiverForScanning(bluetoothDevices, progressDialog);
        activity.registerReceiver(broadcastReceiver, getIntentFilter());
    }

    private BroadcastReceiver getBroadcastReceiverForScanning(final ArrayList<BluetoothDevice> bluetoothDevices,
                                                              final ProgressDialog progressDialog) {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                Log.i(loggerTag, "Action: " + action);

                if (ACTION_DISCOVERY_STARTED.equals(action)) {
                    progressDialog.show();
                } else if (ACTION_DISCOVERY_FINISHED.equals(action)) {
                    progressDialog.dismiss();

                    Intent newIntent = new Intent(activity, BluetoothActivity.class);
                    newIntent.putParcelableArrayListExtra("device.list", bluetoothDevices);
                    Log.i(loggerTag, "Found devices: " + bluetoothDevices);
                    activity.startActivity(newIntent);
                } else if (ACTION_FOUND.equals(action)) {
                    BluetoothDevice device = intent.getParcelableExtra(EXTRA_DEVICE);
                    if (!bluetoothDevices.contains(device)) {
                        bluetoothDevices.add(device);
                        unpairDevice(device);
                        showMessage(context, "Found device " + device.getName());
                    }
                }
            }
        };
    }

    private IntentFilter getIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();

        intentFilter.addAction(ACTION_DISCOVERY_STARTED);
        intentFilter.addAction(ACTION_FOUND);
        intentFilter.addAction(ACTION_DISCOVERY_FINISHED);

        return intentFilter;
    }

    private ProgressDialog getProgressDialog(String title, String message) {
        ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.setProgressStyle(STYLE_HORIZONTAL);
        progressDialog.setIndeterminate(true);
        progressDialog.setMax(100);

        return progressDialog;
    }

    public void pairDevice(BluetoothDevice device) {
        try {
            Method method = device.getClass().getMethod("createBond", (Class[]) null);
            method.invoke(device, (Object[]) null);
            while (true) {
                if (device.getBondState() == BluetoothDevice.BOND_BONDED) {
                    break;
                }
            }
            Intent graphActivityIntent = new Intent(activity, GraphActivity.class);
            graphActivityIntent.putExtra("current.device", device);
            activity.startActivity(graphActivityIntent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void unpairDevice(BluetoothDevice device) {
        try {
            Method method = device.getClass().getMethod("removeBond", (Class[]) null);
            method.invoke(device, (Object[]) null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void unPairDevices(List<BluetoothDevice> bluetoothDevices) {
        for (BluetoothDevice device : bluetoothDevices) {
            unpairDevice(device);
        }
    }

    public void unPairDevices() {
        ArrayList<BluetoothDevice> devices = activity.getIntent().getExtras().getParcelableArrayList("device.list");
        unPairDevices(devices);
    }

    public void showMessage(Context context, String message) {
        MessageManager.showMessage(context, message);
    }

    public void unregisterReceiver() {
        activity.unregisterReceiver(broadcastReceiver);
    }

    public void toGraphView() {
        Intent intent = new Intent(activity, GraphActivity.class);
        activity.startActivity(intent);
    }

    public void clearFoundBluetoothDevices(ArrayList<BluetoothDevice> devices) {
        devices.clear();
    }

    public DataPoint[] getDataPoints() {
        int count = 30;
        DataPoint[] values = new DataPoint[count];
        for (int i = 0; i < count; i++) {
            double x = i;
            double f = mRand.nextDouble() * 0.15 + 0.3;
            double y = Math.sin(i * f + 2) + mRand.nextDouble() * 0.3;
            DataPoint v = new DataPoint(x, y);
            values[i] = v;
        }
        return values;
    }

    double mLastRandom = 2;
    Random mRand = new Random();

    public double getRandom() {
        return mLastRandom += mRand.nextDouble() * 0.5 - 0.25;
    }

    public void startReceiving(Exchanger<Package> packageExchanger) throws IOException {
        Intent intent = activity.getIntent();
        Parcelable object = intent.getExtras().getParcelable("current.device");
        if (object != null) {
            BluetoothDevice currentDevice = (BluetoothDevice) object;
            try {
                bluetoothSocket = currentDevice.createInsecureRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"));
                bluetoothSocket.connect();
                while (true) {
                    if (bluetoothSocket.isConnected()) {
                        break;
                    }
                }
                connectedThread = new ConnectedThread(bluetoothSocket, packageExchanger, composeFileName());
                connectedThread.start();
            } catch (IOException e) {
                e.printStackTrace();
                if (bluetoothSocket != null) {
                    bluetoothSocket.close();
                }
            }
        } else {
            System.out.println("Objects is empty");
        }
    }

    public void stopReceiving() {
        try {
            if (connectedThread != null) {
                connectedThread.closeInputStream();
                connectedThread.closeWriter();
                if (!connectedThread.isInterrupted()) {
                    connectedThread.interrupt();
                }
            }
            if (bluetoothSocket != null) {
                bluetoothSocket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            Intent back = new Intent(activity, BluetoothActivity.class);
            activity.startActivity(back);
        }
    }

    public String composeFileName() {
        Date currentDate = Calendar.getInstance().getTime();
        return String.format("accelerometer_data_%tF_%tT", currentDate, currentDate);
    }
}
