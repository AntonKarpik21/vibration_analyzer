package com.antonkarpenko.vibrationanalyzer.manager;

import android.view.View;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

/**
 * Created by Антон on 14.02.2019.
 */
public class ViewManager {

    public static void disableViews(List<View> views) {
        for (View view : views) {
            disableView(view);
        }
    }

    public static void disableViews(View... views) {
        disableViews(Arrays.asList(views));
    }

    public static void enableViews(View... views) {
        enableViews(Arrays.asList(views));
    }

    public static void enableViews(List<View> views) {
        for (View view : views) {
            enableView(view);
        }
    }

    public static void enableView(View view) {
        view.setVisibility(VISIBLE);
        view.setEnabled(true);
    }

    public static void disableView(View view) {
        view.setVisibility(INVISIBLE);
        view.setEnabled(false);
    }

    public static void setTextToTextView(TextView view, String text) {
        view.setText(text);
    }

    public static void exchangeVisibility(View toDisable, View toEnable) {
        enableView(toEnable);
        disableView(toDisable);
    }
}
