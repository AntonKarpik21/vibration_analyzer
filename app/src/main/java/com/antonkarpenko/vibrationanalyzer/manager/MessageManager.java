package com.antonkarpenko.vibrationanalyzer.manager;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Антон on 07.02.2019.
 */
public class MessageManager {
    public static String BLUETOOTH_ENABLED = "Bluetooth is enabled";
    public static String BLUETOOTH_NOT_SUPPORTED = "Device doesn't support BluetoothActivity";
    public static String NO_PREFERENCE_PARAMETER = "There is no preference parameter: ";

    public static void showMessage(Context context, String textPropOrMessage, int periodOfTime) {
        Toast.makeText(context, textPropOrMessage, periodOfTime).show();
    }

    public static void showMessage(Context context, String textPropOrMessage) {
        showMessage(context, textPropOrMessage, 1);
    }

    public static void showMessage(Context context, String textProp, String value) {
        showMessage(context, textProp.concat(value));
    }
}
