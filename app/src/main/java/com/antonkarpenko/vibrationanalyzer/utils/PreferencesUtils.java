package com.antonkarpenko.vibrationanalyzer.utils;

import android.content.SharedPreferences;

/**
 * Created by Антон on 07.02.2019.
 */
public class PreferencesUtils {

    public static void setProperty(String property, String value, SharedPreferences preferences) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(property, value);
        editor.apply();
    }

    public static String getStringProperty(String property, SharedPreferences preferences) {
        return preferences.getString(property, null);
    }

    public static void setProperty(String property, int value, SharedPreferences preferences) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(property, value);
        editor.apply();
    }

    public static int getIntProperty(String property, SharedPreferences preferences) {
        return preferences.getInt(property, 0);
    }
}
