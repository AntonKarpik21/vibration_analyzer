package com.antonkarpenko.vibrationanalyzer;

import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.antonkarpenko.vibrationanalyzer.dagger2.initialization.App;
import com.antonkarpenko.vibrationanalyzer.manager.ActionManager;

import java.util.ArrayList;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    private TextView topLabel;
    private Button enableBtn;
    private Button pairedDevicesButton;
    private Button scanBtn;
    private String textAfterInit;
    private ArrayList<BluetoothDevice> bluetoothDevices = new ArrayList<>();

    private static final String LOGGER_TAG = "MAIN_ACTIVITY";

    @Inject
    ActionManager actionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initServicesAndViews();
        actionManager.setActivity(this);
        actionManager.setLoggerTag(LOGGER_TAG);
        actionManager.initSharedPreferences();
        actionManager.initBluetoothAdapter();
        actionManager.checkBluetoothActivity(enableBtn, scanBtn, topLabel, textAfterInit);
        actionManager.clearFoundBluetoothDevices(bluetoothDevices);
        actionManager.setScanBluetoothDevicesParameters(bluetoothDevices);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        actionManager.unregisterReceiver();
    }

    private void initServicesAndViews() {
        initWithDagger2();
        enableBtn = (Button) findViewById(R.id.ENABLE);
        scanBtn = (Button) findViewById(R.id.SCAN);
        topLabel = (TextView) findViewById(R.id.STATE_DESCRIPTION);
        pairedDevicesButton = (Button) findViewById(R.id.PAIRED_DEVICES);
        textAfterInit = getString(R.string.description_after_init);
    }

    public void onButtonClick(View v) {
        switch (v.getId()) {
            case R.id.ENABLE:
                actionManager.enableBluetoothClick(getBaseContext(),
                        topLabel,
                        textAfterInit,
                        enableBtn,
                        scanBtn,
                        pairedDevicesButton);
                break;
            case R.id.SCAN:
                actionManager.startDevicesDiscovery();
                break;
            case R.id.PAIRED_DEVICES:
                actionManager.showPairedDevices(getBaseContext());
                break;
            case R.id.EXIT:
                actionManager.exit();
                break;
            case R.id.button_graph:
                actionManager.toGraphView();
                break;
            default:
                break;
        }
    }

    private void initWithDagger2() {
        App.getAppComponent().injectsMainActivity(this);
        App.getAppComponent().initActionManager(actionManager);
    }
}
