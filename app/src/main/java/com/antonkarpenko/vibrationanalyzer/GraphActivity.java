package com.antonkarpenko.vibrationanalyzer;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.antonkarpenko.vibrationanalyzer.dagger2.initialization.App;
import com.antonkarpenko.vibrationanalyzer.manager.ActionManager;
import com.antonkarpenko.vibrationanalyzer.model.GraphThread;
import com.antonkarpenko.vibrationanalyzer.model.Package;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.IOException;
import java.util.concurrent.Exchanger;

import javax.inject.Inject;

/**
 * Created by Антон on 31.03.2019.
 */
public class GraphActivity extends AppCompatActivity {

    private final Handler handler = new Handler();
    private GraphView graphView;
    private LineGraphSeries<DataPoint> lineGraphSeriesX;
    private LineGraphSeries<DataPoint> lineGraphSeriesY;
    private GraphThread graphThread;
    private Exchanger<Package> packageExchanger;

    private static final String LOGGER_TAG = "GRAPH_ACTIVITY";

    @Inject
    ActionManager actionManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initWithDagger2();
        setContentView(R.layout.activity_graph);
        initGraph();
        actionManager.setActivity(this);
        packageExchanger = new Exchanger<>();
    }

    @Override
    protected void onResume() {
        super.onResume();

//        graphView = new Runnable() {
//            @Override
//            public void run() {
//                graph2LastXValue += 1d;
//                lineGraphSeriesX.appendData(new DataPoint(graph2LastXValue, actionManager.getRandom()), true, 40);
//                handler.postDelayed(this, 200);
//            }
//        };

        graphThread = new GraphThread(packageExchanger, lineGraphSeriesX, lineGraphSeriesY);
        handler.postDelayed(graphThread, 1000);
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            actionManager.startReceiving(packageExchanger);
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*LineGraphSeries<DataPoint> series = new LineGraphSeries<>(new DataPoint[] {
                new DataPoint(0, 1),
                new DataPoint(1, 5),
                new DataPoint(2, 3)
        });
        graphView.addSeries(series);*/
    }

    @Override
    protected void onPause() {
        handler.removeCallbacks(graphThread);
        super.onPause();
    }

    public void onButtonClick(View view) {
        switch (view.getId()) {
            case R.id.stop_btn:
                actionManager.stopReceiving();
        }
    }

    private void initWithDagger2() {
        App.getAppComponent().injectsGraphActivity(this);
        App.getAppComponent().initActionManager(actionManager);
    }

    private void initGraph() {
        graphView = findViewById(R.id.graph);
        lineGraphSeriesX = new LineGraphSeries<>();
        lineGraphSeriesY = new LineGraphSeries<>();
        graphView.addSeries(lineGraphSeriesX);
        graphView.addSeries(lineGraphSeriesY);
        lineGraphSeriesX.setColor(Color.RED);
        lineGraphSeriesY.setColor(Color.BLUE);
        graphView.getViewport().setXAxisBoundsManual(true);
//        graphView.getViewport().setMinX(0);
//        graphView.getViewport().setMaxX(100);
        graphView.getViewport().setScalable(true);
        lineGraphSeriesY.setTitle("Y");
        lineGraphSeriesX.setTitle("X");
        graphView.getLegendRenderer().setVisible(true);
        graphView.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);
    }
}
