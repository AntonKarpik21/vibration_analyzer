package com.antonkarpenko.vibrationanalyzer.adapter;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.antonkarpenko.vibrationanalyzer.R;

import static com.antonkarpenko.vibrationanalyzer.constants.ActivityConstants.UNPAIR;
import static com.antonkarpenko.vibrationanalyzer.constants.ActivityConstants.PAIR;

import java.util.List;

/**
 * Created by Антон on 17.02.2019.
 */
public class ListViewAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private List<BluetoothDevice> devices;
    private OnPairButtonClickListener listener;

    public ListViewAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
    }

    public LayoutInflater getLayoutInflater() {
        return layoutInflater;
    }

    public void setLayoutInflater(LayoutInflater layoutInflater) {
        this.layoutInflater = layoutInflater;
    }

    public List<BluetoothDevice> getDevices() {
        return devices;
    }

    public void setDevices(List<BluetoothDevice> devices) {
        this.devices = devices;
    }

    public OnPairButtonClickListener getListener() {
        return listener;
    }

    public void setListener(OnPairButtonClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return devices == null ? 0 : devices.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.activity_list_item, null);
            viewHolder = new ViewHolder();

            viewHolder.setDeviceName((TextView) convertView.findViewById(R.id.tv_name));
            viewHolder.setAddress((TextView) convertView.findViewById(R.id.tv_address));
            viewHolder.setPairButton((TextView) convertView.findViewById(R.id.btn_pair));

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        BluetoothDevice device = devices.get(position);

        viewHolder.getDeviceName().setText(device.getName());
        viewHolder.getAddress().setText(device.getAddress());
        viewHolder.getPairButton().setText((device.getBondState() == BluetoothDevice.BOND_BONDED) ? UNPAIR : PAIR);

        viewHolder.getPairButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onPairButtonClick(position);
                }
            }
        });

        return convertView;
    }

    static class ViewHolder {
        private TextView deviceName;
        private TextView address;
        private TextView pairButton;

        public TextView getDeviceName() {
            return deviceName;
        }

        public void setDeviceName(TextView deviceName) {
            this.deviceName = deviceName;
        }

        public TextView getAddress() {
            return address;
        }

        public void setAddress(TextView address) {
            this.address = address;
        }

        public TextView getPairButton() {
            return pairButton;
        }

        public void setPairButton(TextView pairButton) {
            this.pairButton = pairButton;
        }
    }

    public interface OnPairButtonClickListener {
        void onPairButtonClick(int position);
    }
}
