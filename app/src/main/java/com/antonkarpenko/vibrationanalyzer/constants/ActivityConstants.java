package com.antonkarpenko.vibrationanalyzer.constants;

/**
 * Created by Антон on 06.02.2019.
 */
public class ActivityConstants {
    public final static String PAIR = "Pair";
    public final static String UNPAIR = "Unpair";
}
