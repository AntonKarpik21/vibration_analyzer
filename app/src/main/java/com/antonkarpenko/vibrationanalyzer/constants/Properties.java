package com.antonkarpenko.vibrationanalyzer.constants;

import android.content.SharedPreferences;

import static com.antonkarpenko.vibrationanalyzer.utils.PreferencesUtils.setProperty;
import static com.antonkarpenko.vibrationanalyzer.utils.PreferencesUtils.getStringProperty;

/**
 * Created by Антон on 07.02.2019.
 */
public class Properties {
    public static String BLUETOOTH_PARAMETERS = "bluetooth.parameters";

    public static void saveBluetoothParameters(String value, SharedPreferences preferences) {
        setProperty(BLUETOOTH_PARAMETERS, value, preferences);
    }

    public static String getBluetoothParameters(SharedPreferences preferences) {
        return getStringProperty(BLUETOOTH_PARAMETERS, preferences);
    }
}
