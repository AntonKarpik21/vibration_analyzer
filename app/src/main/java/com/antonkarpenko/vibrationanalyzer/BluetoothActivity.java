package com.antonkarpenko.vibrationanalyzer;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.TextView;

import com.antonkarpenko.vibrationanalyzer.adapter.ListViewAdapter;
import com.antonkarpenko.vibrationanalyzer.dagger2.initialization.App;
import com.antonkarpenko.vibrationanalyzer.manager.ActionManager;

import java.util.ArrayList;

import javax.inject.Inject;

public class BluetoothActivity extends AppCompatActivity {

    @Inject
    ActionManager actionManager;

    TextView bluetoothName;
    TextView bluetoothAddress;
    TextView bluetoothScanMode;
    TextView bluetoothState;

    ListView deviceList;

    private ArrayList<BluetoothDevice> bluetoothDevices;
    private ListViewAdapter adapter;

    private static final String LOGGER_TAG = "BLUETOOTH_ACTIVITY";

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);

        initWithDagger2();
        actionManager.setActivity(this);
        actionManager.setLoggerTag(LOGGER_TAG);
        actionManager.initSharedPreferences();
        fillBluetoothData();

        bluetoothDevices = getIntent().getExtras().getParcelableArrayList("device.list");
        deviceList = (ListView) findViewById(R.id.device_list);
        adapter = new ListViewAdapter(this);
        adapter.setDevices(bluetoothDevices);
        adapter.setListener(new ListViewAdapter.OnPairButtonClickListener() {
            @Override
            public void onPairButtonClick(int position) {
                BluetoothDevice device = bluetoothDevices.get(position);

                if (device.getBondState() == BluetoothDevice.BOND_BONDED) {
                    actionManager.unpairDevice(device);
                } else {
                    actionManager.showMessage(getBaseContext(), "Pairing...");
                    actionManager.pairDevice(device);
                }
            }
        });

        deviceList.setAdapter(adapter);
        registerReceiver(receiver, new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED));
    }

    @Override
    protected void onResume() {
        super.onResume();
        actionManager.setActivity(this);
        actionManager.setLoggerTag(LOGGER_TAG);
    }

    private void fillBluetoothData() {
        bluetoothName = (TextView) findViewById(R.id.bt_name);
        bluetoothAddress = (TextView) findViewById(R.id.bt_address);
        bluetoothScanMode = (TextView) findViewById(R.id.bt_mode);
        bluetoothState = (TextView) findViewById(R.id.bt_state);

        actionManager.initBluetoothActivity(bluetoothName, bluetoothAddress, bluetoothScanMode, bluetoothState, getBaseContext());
    }

    private void initWithDagger2() {
        App.getAppComponent().injectsBluetoothActivity(this);
        App.getAppComponent().initActionManager(actionManager);
    }

    public ArrayList<BluetoothDevice> getBluetoothDevices() {
        if (bluetoothDevices == null) {
            bluetoothDevices = new ArrayList<>();
        }
        return bluetoothDevices;
    }

    public void setBluetoothDevices(ArrayList<BluetoothDevice> bluetoothDevices) {
        this.bluetoothDevices = bluetoothDevices;
    }

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                final int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR);
                final int prevState = intent.getIntExtra(BluetoothDevice.EXTRA_PREVIOUS_BOND_STATE, BluetoothDevice.ERROR);

                if (state == BluetoothDevice.BOND_BONDED && prevState == BluetoothDevice.BOND_BONDING) {
                    actionManager.showMessage(getBaseContext(), "Paired");
                } else if (state == BluetoothDevice.BOND_NONE && prevState == BluetoothDevice.BOND_BONDED) {
                    actionManager.showMessage(getBaseContext(), "Unpaired");
                }

                adapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        actionManager.unPairDevices();
    }
}
