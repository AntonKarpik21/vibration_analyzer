package com.antonkarpenko.vibrationanalyzer.dagger2.initialization;

import android.app.Application;

import com.antonkarpenko.vibrationanalyzer.dagger2.component.AppComponent;
import com.antonkarpenko.vibrationanalyzer.dagger2.component.DaggerAppComponent;

/**
 * Created by Антон on 06.02.2019.
 */
public class App extends Application {

    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.create();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
