package com.antonkarpenko.vibrationanalyzer.dagger2.modules;

import com.antonkarpenko.vibrationanalyzer.manager.ActionManager;
import com.antonkarpenko.vibrationanalyzer.manager.JsonMapper;
import com.antonkarpenko.vibrationanalyzer.manager.factories.ResourceFactory;
import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Антон on 06.02.2019.
 */
@Module
public class ManagerModule {

    @Provides
    @Singleton
    public ActionManager provideActionManager() {
        return new ActionManager();
    }

    @Provides
    @Singleton
    public JsonMapper provideJsonMapper() {
        return new JsonMapper(new Gson());
    }

    @Provides
    @Singleton
    public ResourceFactory provideResourceFactory() {
        return new ResourceFactory();
    }
}
