package com.antonkarpenko.vibrationanalyzer.dagger2.component;

import com.antonkarpenko.vibrationanalyzer.BluetoothActivity;
import com.antonkarpenko.vibrationanalyzer.GraphActivity;
import com.antonkarpenko.vibrationanalyzer.MainActivity;
import com.antonkarpenko.vibrationanalyzer.dagger2.modules.ManagerModule;
import com.antonkarpenko.vibrationanalyzer.manager.ActionManager;
import com.antonkarpenko.vibrationanalyzer.manager.ViewManager;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Антон on 06.02.2019.
 */
@Singleton
@Component(modules = ManagerModule.class)
public interface AppComponent {
    void injectsMainActivity(MainActivity mainActivity);
    void injectsBluetoothActivity(BluetoothActivity bluetoothActivity);
    void injectsGraphActivity(GraphActivity graphActivity);

    void initActionManager(ActionManager actionManager);
}
